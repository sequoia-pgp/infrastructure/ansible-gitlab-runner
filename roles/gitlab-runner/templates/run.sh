{% include "base.sh" %}

VM_IP=$(_get_vm_ip)

ssh -i /tmp/${VM_ID}_ed25519 -o StrictHostKeyChecking=no debian@"$VM_IP" /bin/bash < "${1}"
if [ $? -ne 0 ]; then
    # Exit using the variable, to make the build as failure in GitLab
    # CI.
    exit "$BUILD_FAILURE_EXIT_CODE"
fi
