{% include "base.sh" %}

# Destroy VM.
virsh shutdown "$VM_ID" ; virsh destroy "$VM_ID" || true

# Undefine VM.
virsh undefine "$VM_ID" || true

rm -fv /tmp/${VM_ID}_ed25519 /tmp/${VM_ID}_ed25519.pub ${VM_IMAGES_PATH}/${VM_ID}*
